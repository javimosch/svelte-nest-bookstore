import { Controller, Get, Param } from '@nestjs/common';

@Controller('users')
export class UsersController {

    @Get()
    getUsers():any{
        return [{name:"Foo"}]
    }

    @Get(':id')
    getUserById(@Param('id') id:String):any{
        return {
            id:Number(id),
            name:"FOO"
        }
    }

}
