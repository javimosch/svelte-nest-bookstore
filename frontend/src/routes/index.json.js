
/**
 * @type {import('@sveltejs/kit').RequestHandler}
 */
export async function get({ params }) {
	// the `slug` parameter is available because this file
	// is called [slug].json.js
	const { slug } = params;

	
		return {
			body: {
				type: await (await fetch(`${import.meta.env.VITE_BACKEND_URL}/books`)).json()
			}
		};
	
}