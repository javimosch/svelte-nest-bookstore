import adapter from '@sveltejs/adapter-node';
import legacy from '@vitejs/plugin-legacy';
import { optimizeImports, optimizeCss } from 'carbon-preprocess-svelte';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	compilerOptions: {
		hydratable: true
	},
	kit: {
		ssr: true,
		hydrate: true,
		target: '#svelte',
		adapter: adapter({
			// default options are shown
			out: 'build',
			precompress: true
		})
	},
	vite: {
		plugins: [
			optimizeImports(), //carbon
			legacy({
				targets: ['ie >= 11'],
				additionalLegacyPolyfills: ['regenerator-runtime/runtime']
			}),
			{
				name: 'inject-envs',
				enforce: 'pre',
				buildStart() {
					console.log('Loading envs');
					require('dotenv').config({ silent: true });
				}
			},
			process.env.NODE_ENV === 'production' && optimizeCss()
		],
		build: { minify: process.env.NODE_ENV === 'production' }
	}
};

export default config;
